"""
-------------------------------
Pengwing - eBay flipper manager
      2016 Fiona & Alice
-------------------------------

Template filters
"""

from flask import Markup

def nl2br(s):
    return Markup(s.replace("\n", "<br />"))
