"""
-------------------------------
Pengwing - eBay flipper manager
      2016 Fiona & Alice
-------------------------------

The web interface for interacting with Pengwing
"""

import os
from datetime import datetime, timedelta
from flask import Flask, render_template, request, flash, redirect, url_for
from flask_wtf import Form
from wtforms import StringField, IntegerField, FloatField, BooleanField
from wtforms.validators import DataRequired
from sqlalchemy.orm.exc import NoResultFound
from pengwing_core import core
from pengwing_core import settings as pengwing_settings
from pengwing_core.models import Config, ConfigType, SearchQuery, EBayItem, EBayItemState
import pengwing_web.filters as filters
from pengwing_web.tables import SearchesTable, ItemQueueTable, BidReviewTable, \
     BidReviewSuccessTable, BidReviewFailedTable
from pengwing_web.forms import SearchesForm

# Set up Flask app
app = Flask('pengwing_web')
app.jinja_env.filters['nl2br'] = filters.nl2br
app.secret_key = pengwing_settings.SECRET_KEY

# Global template context
@app.context_processor
def inject_num_in_item_queue():
    return {
        'num_in_item_queue' : core.db_session.query(EBayItem).filter_by(state=EBayItemState.NEW).count()
        }

# ------------------
# Front page
# ------------------
@app.route("/")
def index():
    readme = ""
    with open(os.path.join("README.txt")) as f:
        readme = f.read()
    return render_template('index.html', page='index', readme=readme)

# ------------------
# Configuration
# ------------------
class ConfigForm(Form):
    pass

@app.route("/config", methods=["POST", "GET"])
def config():
    # Dynamically build form
    config = core.db_session.query(Config).all()
    for c in config:
        field = None
        if c.type == ConfigType.string:
            field = StringField(c.full_name, validators=[DataRequired()])
        elif c.type == ConfigType.int:
            field = IntegerField(c.full_name, validators=[DataRequired()])
        elif c.type == ConfigType.float:
            field = FloatField(c.full_name, validators=[DataRequired()])
        elif c.type == ConfigType.int:
            field = BooleanField(c.full_name)
        setattr(ConfigForm, c.id, field)
    form = ConfigForm()

    # Save new config and redirect back
    if form.validate_on_submit():
        for c in config:
            c.value = getattr(form, c.id).data
        core.db_session.commit()
        flash("Saved configuration", "success")
        return redirect(url_for('config'))

    # Set current vals in form
    for c in config:
        getattr(form, c.id).data = c.value
    return render_template('config.html', page='config', form=form)

# ------------------
# Searches
# ------------------
@app.route("/searches")
def searches():
    searches = core.db_session.query(SearchQuery).all()
    table = SearchesTable(searches)
    return render_template('searches.html', page='searches', table=table)

@app.route("/searches/add", methods=["POST", "GET"])
def searches_add():
    form = SearchesForm()
    if form.validate_on_submit():
        core.db_session.add(
            SearchQuery(
                query=form.query.data,
                max_bid=int(form.max_bid.data) * 100,
                last_searched=datetime.utcnow() - timedelta(hours=1)
              )
            )
        core.db_session.commit()
        flash("Added search", "success")
        return redirect(url_for('searches'))
    return render_template('searches_add.html', page='searches', form=form)

@app.route("/searches/<int:id>", methods=["POST", "GET"])
def searches_edit(id):
    search = core.db_session.query(SearchQuery).get(id)
    if not search:
        flash("Search passed not found", "danger")
        return redirect(url_for("searches"))
    form = SearchesForm(data={
          'query':search.query,
          'max_bid':int(search.max_bid / 100)
        })
    if form.validate_on_submit():
        search.query = form.query.data
        search.max_bid = int(form.max_bid.data) * 100
        core.db_session.commit()
        flash("Edited search", "success")
        return redirect(url_for("searches"))
    return render_template('searches_edit.html', page='searches', form=form)

@app.route("/searches/delete/<int:id>")
def searches_delete(id):
    search = core.db_session.query(SearchQuery).get(id)
    if not search:
        flash("Search passed not found", "danger")
    else:
        core.db_session.delete(search)
        core.db_session.commit()
        flash("Search deleted", "success")
    return redirect(url_for("searches"))

# ------------------
# Item Queue
# ------------------
@app.route("/item_queue", methods=["POST", "GET"])
def queue():
    items = core.db_session.query(EBayItem).filter_by(state=EBayItemState.NEW, hide=False).order_by(EBayItem.search_query_id).all()

    # If we're trying to curate
    if request.method == "POST":
        for item in items:
            # Final check it's not going to go through again
            if not item.state == EBayItemState.NEW:
                continue
            # Accepted item
            if ("accept_" + item.id) in request.form and \
                   request.form['accept_' + item.id] == 'on' and \
                   ('max_bid_' + item.id) in request.form:
                item.state = EBayItemState.ACCEPTED
                item.max_bid = int(int(request.form['max_bid_' + item.id]) * 100)
            # Rejected item
            elif ("reject_" + item.id) in request.form and \
                   request.form['reject_' + item.id] == 'on' and \
                   ('max_bid_' + item.id) in request.form:
                item.state = EBayItemState.REJECTED
                item.max_bid = int(int(request.form['max_bid_' + item.id]) * 100)
            core.db_session.commit()
        flash("Curated items", "success")
        return redirect(url_for("queue"))
    table = ItemQueueTable(items)
    return render_template('item_queue.html', page='queue', table=table)

# ------------------
# Bid Review
# ------------------
@app.route("/bid_review", methods=["POST", "GET"])
def bid_review():
    items = core.db_session.query(EBayItem).filter_by(state=EBayItemState.ACCEPTED).filter_by(hide=False).order_by(EBayItem.end_time.asc()).all()
    # If we're trying to update
    if request.method == "POST":
        for item in items:
            # Rejected item
            if ("reject_" + item.id) in request.form and \
                   request.form['reject_' + item.id] == 'on':
                item.state = EBayItemState.REJECTED
            # Updating bid
            if ('max_bid_' + item.id) in request.form:
                item.max_bid = int(int(request.form['max_bid_' + item.id]) * 100)
            core.db_session.commit()
        flash("Updated items", "success")
        return redirect(url_for("bid_review"))
    table = BidReviewTable(items)
    return render_template('bid_review.html', page='bid_review', sub_page="future", table=table)

@app.route("/bid_review/success", methods=["POST", "GET"])
def bid_review_success():
    items = core.db_session.query(EBayItem).filter_by(state=EBayItemState.ACCEPTED_WON).filter_by(hide=False).order_by(EBayItem.end_time.asc()).all()
    # If we're trying to hide items
    if request.method == "POST":
        for item in items:
            item.hide = True
            core.db_session.commit()
        flash("Items hidden", "success")
        return redirect(url_for("bid_review_success"))
    table = BidReviewSuccessTable(items)
    return render_template('bid_review_success.html', page='bid_review', sub_page="success", table=table)

@app.route("/bid_review/failed", methods=["POST", "GET"])
def bid_review_failed():
    items = core.db_session.query(EBayItem).filter_by(state=EBayItemState.ACCEPTED_DID_NOT_WIN).filter_by(hide=False).order_by(EBayItem.end_time.asc()).all()
    # If we're trying to hide items
    if request.method == "POST":
        for item in items:
            item.hide = True
            core.db_session.commit()
        flash("Items hidden", "success")
        return redirect(url_for("bid_review_failed"))
    table = BidReviewFailedTable(items)
    return render_template('bid_review_failed.html', page='bid_review', sub_page="failed", table=table)

if __name__ == '__main__':
    app.run()
