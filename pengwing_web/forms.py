"""
-------------------------------
Pengwing - eBay flipper manager
      2016 Fiona & Alice
-------------------------------

Form classes in the web interface
"""

from flask import Markup
from flask_wtf import Form
from wtforms import StringField, IntegerField, DecimalField, BooleanField, SubmitField
from wtforms.validators import DataRequired



class CurrencyField(IntegerField):
    def __call__(self, **kwargs):
        kwargs['class'] = "form-control"
        return Markup('<div class="input-group" style="width:150px;"> ' +
                      '<div class="input-group-addon">&pound;</div>' +
                      super(CurrencyField, self).__call__(**kwargs) +
                      '<div class="input-group-addon">.00</div>' +
                      '</div>')

class SearchesForm(Form):
    query = StringField("Search keywords", validators=[DataRequired()])
    max_bid = CurrencyField("Maximum bid", validators=[DataRequired()])
