"""
-------------------------------
Pengwing - eBay flipper manager
      2016 Fiona & Alice
-------------------------------

Tables that display on the site
"""

from flask import url_for
from flask_table import Table, Col, DatetimeCol
from pengwing_core.models import EBayItem

class PenceToPoundsCol(Col):
    def td_format(self, content):
        return "&pound;{0:.2f}".format(content / 100.0)

class ActionCol(Col):
    def td_contents(self, i, attr_list):
        return '<a href="' + url_for('searches_edit', id=i.id) + '" class="btn btn-secondary btn-sm">Edit</a>' \
               '<a href="' + url_for('searches_delete', id=i.id) + '" class="btn btn-secondary btn-sm">Delete</a>'

class ImageCol(Col):
    def td_format(self, content):
        if content:
            return '<img src="' + content + '" />'
        return "-"

class ItemInfoCol(Col):
    def td_contents(self, i, attr_list):
        # Titles
        content = '<strong><a href="' + i.item_url + '">' + i.title + '</a></strong>'
        content += ' (' + i.country + ')<br />'
        content += EBayItem.listing_type_names[i.listing_type]
        if i.subtitle:
            content += ' - ' + i.subtitle

        # Cost
        content += "<br /><strong>&pound;{0:.2f}".format((i.current_price + i.shipping_price) / 100.0) + "</strong> (inc p&amp;p)"
        if i.listing_type == "AuctionWithBIN" and i.buy_it_now:
            content += "<br />BIN: " + "<strong>&pound;{0:.2f}".format((i.buy_it_now_price + i.shipping_price) / 100.0) + "</strong> (inc p&amp;p)"

        return content

class CurateColumn(Col):
    def td_contents(self, i, attr_list):
        content = '<div style="width:100px;">'
        content += '<label><img src="' + url_for("static", filename="img/cross.png") + '" /><input type="checkbox" name="reject_' + i.id + '"></label>'
        content += '<label><img src="' + url_for("static", filename="img/tick.png") + '" /><input type="checkbox" name="accept_' + i.id + '"></label>'
        content += ('<div class="input-group"> ' +
                    '<div class="input-group-addon" style="width:0px;">&pound;</div>' +
                    '<input type="text" value="' + str(int((i.search_query.max_bid - i.shipping_price)/ 100.0)) + '" name="max_bid_' + i.id + '" style="width:40px;"/>' +
                    '</div>')
        content += '</div>'
        return content

class UpdateBidColumn(Col):
    def td_contents(self, i, attr_list):
        content = '<div style="width:100px;">'
        content += '<label>Cancel bid <input type="checkbox" name="reject_' + i.id + '"></label>'
        content += ('<div class="input-group"> ' +
                    '<div class="input-group-addon" style="width:0px;">&pound;</div>' +
                    '<input type="text" value="' + str(int((i.max_bid)/ 100.0)) + '" name="max_bid_' + i.id + '" style="width:40px;"/>' +
                    '</div>')
        content += '</div>'
        return content

class SearchesTable(Table):
    classes = ["table", "table-striped"]

    query = Col("Search Keywords")
    last_searched = DatetimeCol("Last Searched")
    max_bid = PenceToPoundsCol("Maximum Bid")
    actions = ActionCol("Actions")

class ItemQueueTable(Table):
    classes = ["table", "table-striped"]

    image_url = ImageCol("Image")
    item_info = ItemInfoCol("Description / Current Price")
    end_time = DatetimeCol("Ends")
    curate = CurateColumn("Bid on?")

class BidReviewTable(Table):
    classes = ["table", "table-striped"]

    image_url = ImageCol("Image")
    item_info = ItemInfoCol("Description / Current Price")
    end_time = DatetimeCol("Ends")
    curate = UpdateBidColumn("Update")

class BidReviewSuccessTable(Table):
    classes = ["table", "table-striped"]

    image_url = ImageCol("Image")
    item_info = ItemInfoCol("Description / Current Price")
    end_time = DatetimeCol("Finished")

class BidReviewFailedTable(BidReviewSuccessTable):
    pass
