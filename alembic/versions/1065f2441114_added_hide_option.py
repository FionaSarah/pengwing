"""added hide option

Revision ID: 1065f2441114
Revises: e940b80fbb15
Create Date: 2016-06-06 17:23:48.807509

"""

# revision identifiers, used by Alembic.
revision = '1065f2441114'
down_revision = 'e940b80fbb15'
branch_labels = None
depends_on = None

from alembic import op
import sqlalchemy as sa


def upgrade():
    ### commands auto generated by Alembic - please adjust! ###
    op.add_column('ebayitem', sa.Column('hide', sa.Boolean(), nullable=True))
    ### end Alembic commands ###


def downgrade():
    ### commands auto generated by Alembic - please adjust! ###
    op.drop_column('ebayitem', 'hide')
    ### end Alembic commands ###
