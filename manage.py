#!/usr/bin/env python
"""
-------------------------------
Pengwing - eBay flipper manager
      2016 Fiona & Alice
-------------------------------

This management tool is used for development, initial setup and
querying. See README.txt for usage instructions.
"""

import sys
import argparse
import subprocess
from sqlalchemy import create_engine
from sqlalchemy.engine import reflection
from sqlalchemy.orm import sessionmaker
from pengwing_core import settings
from pengwing_core.models import Base, Config, SearchQuery


class PengwingManagerApp:

    def __init__(self):
        main_parser = argparse.ArgumentParser(description='High-level management tool for Pengwing.')
        subparsers = main_parser.add_subparsers(help='Pengwing tasks')

        parser_1 = subparsers.add_parser('init_database', help='Initialises all database tables.')
        parser_1.set_defaults(func=self.task_init_database)
        parser_2 = subparsers.add_parser('test_bot', help='Runs the bot from here for testing and development purposes.')
        parser_2.set_defaults(func=self.task_test_bot)
        parser_2 = subparsers.add_parser('test_web', help='Runs the web server for testing and development purposes.')
        parser_2.set_defaults(func=self.task_test_web)
        parser_3 = subparsers.add_parser('create_migration', help='Generate new database migration.')
        parser_3.add_argument('message', help='Short description for new migration.')
        parser_3.set_defaults(func=self.task_create_migration)
        parser_4 = subparsers.add_parser('run_migrations', help='Runs all migrations.')
        parser_4.set_defaults(func=self.task_run_migrations)

        if len(sys.argv[1:])==0:
            main_parser.print_help()
            main_parser.exit()
        args = main_parser.parse_args()
        args.func(args)

    def task_init_database(self, parsed_args):
        """Creates the default database structure in postgres"""
        print("Connecting to database. . .")
        engine, session = self.get_db_engine_and_session()
        insp = reflection.Inspector.from_engine(engine)
        if "config" in insp.get_table_names():
            print("Tables already created. . .")
            return
        print("Attempting to create initial tables. . .")
        Base.metadata.create_all(engine)
        print("Creating initial data. . .")
        for config_item in Config.config_values():
            config = Config(**config_item)
            session.add(config)
        session.add(SearchQuery(query="playstation 3", max_bid=30*100))
        session.add(SearchQuery(query="ath m50x", max_bid=50*100))
        session.commit()

    def task_test_bot(self, parsed_args):
        """Runs the bot from here for testing and dev purposes, should be ran directiy as
        a daemon though."""
        print("Starting bot. . .")
        print("CTRL+C to kill.")
        from pengwing_bot.__main__ import PengwingBot
        PengwingBot()

    def task_test_web(self, parsed_args):
        """Runs the web interface for testing and dev purposes."""
        print("Starting web interface. . .")
        print("CTRL+C to kill.")
        from pengwing_web.app import app
        app.run(debug=True)

    def task_create_migration(self, parsed_args):
        """Creates a new alembic migration"""
        subprocess.call(["alembic", "revision", "--autogenerate", "-m", parsed_args.message])

    def task_run_migrations(self, parsed_args):
        """Updates the database to the latest version."""
        subprocess.call(["alembic", "upgrade", "head"])

    def get_db_engine_and_session(self):
        engine = create_engine(settings.DATABASE_PATH)
        Base.metadata.bind = engine
        session = sessionmaker(bind=engine)()
        return engine, session

if __name__ == "__main__":
    PengwingManagerApp()
