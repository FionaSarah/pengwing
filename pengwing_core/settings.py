DATABASE_PATH = "postgresql+psycopg2://pengwing:pengwing@localhost:5432/pengwing"
SECRET_KEY = "ter$tnjucreoprtkjom<@NT$Y9y6"

EBAY_ENVIRONMENT = "production"

EBAY_GLOBAL_ID = 'EBAY-GB'

EBAY_CREDENTIALS = {'sandbox' : {
    'app_id': 'FionaSum-Pengwing-SBX-88a129233-4864989f',
    'dev_id': '95db837f-2861-430d-9fea-8ad2dc0a6c13',
    'cert_id': 'SBX-8a129233fc78-0840-42d7-b7d6-9bd9',
    'token' : "AgAAAA**AQAAAA**aAAAAA**DIpNVw**nY+sHZ2PrBmdj6wVnY+sEZ2PrA2dj6wFk4GgDpOAqA+dj6x9nY+seQ**tNQDAA**AAMAAA**mVMRiYJrfvyc0d2KF2gOpPix/eH/6tZ0/3gFShwv1HVOoRGpgfpEQenMn+iyGBIcSBsXfSqTxfEd3StYwQejq37pTFCl4BC1dTOHRdDlyisDs95abzXGRIe5tXS00ga4KWThCEoeI6jQIyx07S52mz7wAjluaw1YJlQGAAeq3WBakxFSSQDGhcdbvenCS5WBNLaSQFvOuOpvVi7Vb2ZKXvt9tnQHGVW2XYO4Novp31nRZO6r+I8jjfWKV/UFQXqqtNCbnRoV16a1eXe+dvwK2XO9wFGyrDlXFKhNOetEO1OcjhjLxTpp4zI0JvpXsAT3ovzIvJ0sAbM7VFLzI224H3vb9NiVGUT/OWZuV1GajuUIAYwHTc7LTIsw9aLU4vvDWiF+xCDlkYTtAz+nJPertEPBBkCpTTVrLdo7AC1pjXJMrUJQF0lgHDxdfLCAkEHG3IMq876ZSlzJ/8u9FiQaBBOY29b4ffTTRxaHmeigrVxSjut5wAleMzZbgMkhetIwFzSulAMw2AKK4r0Yg0CRZUAUvsdZUb725cHGQwJ6RRnf4jb2mfSguA1Q9r44HWvxNu4g4icZnyiMCgNHZXdAd6FfOqCHMMchK41d86h6jl084PQT74+3q89vH50G1qhY0movm2NFhg1Vjf3QyPgAGe8YPwWBJc1gk89cwwUfXJHT5z8qr3Cf6AfOWA6FzXj5X03rqSG3hNRgY+WhLAXLBy3aIstcogqX9SGLzh0TraL6gj3hEZMbSewF6YK3jxPY"
    },
    'production' : {
        'app_id':'FionaSum-Pengwing-PRD-f2f871c7c-ba3a944a',
        'dev_id':'',
        'cert_id':'',
        'token':'AgAAAA**AQAAAA**aAAAAA**7Y9NVw**nY+sHZ2PrBmdj6wVnY+sEZ2PrA2dj6wFk4amDZeLqQidj6x9nY+seQ**qk4DAA**AAMAAA**jDZnP3fsHPjdsSPweSM8JekEl6fOwHnmyS8+/vvuXh3m7FcnVgPZFaSgDsZZAVSaH/wjLLOlr6KuafvyQj5+aiIf5bQt98qPmIBDT4chBBjU4ys4hrqnG6l6eL+PjRy0RPyYgrN5A2AFOAyPK3zR9ouehlgrzhCZ8f86oRS00XYD9fP9Jsl501uLztjwv/zneX1SsYUikHUllyja46/OlWC1fE+nfhR/5l8D/APO/slgSBKmtxgsKKxIFn0hw+kqysiUlz2kDmxqQJGRjeBSbXJNy8QiwIH8+K2Y7yYQ+0OV/fM4Hlx7JRAgarl9lnMKcyBdXcdCI56/IAU/pUGXuaP6NorQr/TUsMOL/yF4ybfTtwzR2jenQwvFmOOhdPIbFg8/egc9Rrl7x8o+617CjdHo+y7fjA9L166WRTh1dtY8Z9dn9jdMEhnxGYREflw5DIXtd1eDZ/wWdZI5g7ZW6cWtYXIyTDGnAxCgxX7x3qCYQQEMAH6vU0E67nsW9oxNks5KNUl6sfQFpzKE/SeqCqwVUtXbicdLgkk0BZ0NQMj/Qg6ifk9bGcqiGh3vzLC7laGbw3FuuGgwSBwhU3AyHMrNjkm7T0V2zua+GdeHsS5ndANvlK++GS+5n6jINT7P9yQFSlteCAySyLMNPLcndykGehJ4J011Wq5JJ2ZBe2mvrLGf2IQ/wEqO/rxV4a/SSHw7zufyKLw0DrJBoV/sWgIbzEZuF9Z28mSCX7cXJDi4LXTvMWMCGhbpobxxtT3n'
    }
}

EBAY_FINDING_API = {'sandbox': 'http://svcs.sandbox.ebay.co.uk', 'production' : 'http://svcs.ebay.co.uk'}
EBAY_MECHANDISING_API = {'sandbox': 'http://svcs.sandbox.ebay.co.uk', 'production' : 'http://svcs.ebay.co.uk'}
EBAY_SHOPPING_API = {'sandbox': 'http://open.api.sandbox.ebay.co.uk', 'production' : 'http://open.api.ebay.co.uk'}
EBAY_TRADING_API = {'sandbox': 'https://api.sandbox.ebay.co.uk', 'production' : 'https://api.ebay.co.uk'}
