"""
-------------------------------
Pengwing - eBay flipper manager
      2016 Fiona & Alice
-------------------------------

Helper classes or methods.
"""

class Borg:
    """Singleton implementation, inheret from it."""
    _shared_state = {}
    def __init__(self):
        self.__dict__ = self._shared_state
