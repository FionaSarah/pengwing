"""
-------------------------------
Pengwing - eBay flipper manager
      2016 Fiona & Alice
-------------------------------

All the database models for Pengwing live here.
"""

import datetime
from sqlalchemy import Column, ForeignKey, Integer, String, \
     Text, DateTime, Boolean
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship


Base = declarative_base()

class ConfigType:
    string = "string"
    int = "int"
    float = "float"
    boolean = "boolean"

class Config(Base):
    __tablename__ = 'config'
    id = Column(String(250), nullable=False, primary_key=True)
    full_name = Column(String(250), nullable=False)
    value = Column(Text())
    type = Column(String(16), nullable=False, default=ConfigType.int)

    @classmethod
    def config_values(cls):
        return [
            {
                'id' : 'search_interval',
                'full_name' : 'Minutes between eBay searches for listings',
                'value' : '30',
                'type' : ConfigType.int
            },
        ]

class SearchQuery(Base):
    __tablename__ = 'searchquery'
    id = Column(Integer, primary_key=True)
    query = Column(String(255), nullable=False)
    last_searched = Column(DateTime, default=datetime.datetime.utcnow)
    max_bid = Column(Integer, nullable=False)
    ebay_items = relationship('EBayItem', backref='search_query', lazy='joined')

    def __str__(self):
        return "SearchQuery [id:{}, query:{}, max_bid:{}, last_searched:{}]".format(
            self.id, self.query, self.max_bid, self.last_searched
            )

class EBayItemState:
    NEW = 0
    ACCEPTED = 1
    REJECTED = 2
    ACCEPTED_WON = 3
    ACCEPTED_DID_NOT_WIN = 4

class EBayItem(Base):
    __tablename__ = 'ebayitem'
    id = Column(String(255), nullable=False, primary_key=True)
    search_query_id = Column(Integer, ForeignKey('searchquery.id'))
    state = Column(Integer, default=EBayItemState.NEW)
    hide = Column(Boolean, default=False)

    max_bid = Column(Integer, default=0)
    item_url = Column(String(255), nullable=True)
    title = Column(String(255), nullable=True)
    subtitle = Column(String(255), nullable=True)
    image_url = Column(String(255), nullable=True)
    country = Column(String(2), nullable=True)
    buy_it_now = Column(Boolean, default=False)
    buy_it_now_price = Column(Integer, default=0)
    listing_type = Column(String(32), nullable=True)
    start_time = Column(DateTime, nullable=True)
    end_time = Column(DateTime, nullable=True)
    bid_count = Column(Integer, default=0)
    current_price = Column(Integer, default=0)
    shipping_type = Column(String(64), nullable=True)
    shipping_price = Column(Integer, default=0)

    listing_type_names = {
        'AdFormat' : "Advertisement",
        'Auction' : "Auction",
        'AuctionWithBIN' : "Auction + Buy It Now",
        'Classified' : "Classified",
        'FixedPrice' : "Buy It Now",
        'StoreInventory' : "Store Inventory"
        }
    shipping_type_names = {
        'Calculated' : "Calculated",
        'CalculatedDomesticFlatInternational' : "Calculated Domestic/Flat International",
        'Flat' : "Flat",
        'FlatDomesticCalculatedInternational' : "Flat Domestic/Calculated International",
        'Free' : "Free Postage",
        'FreePickup' : "Pick-up Only",
        'Freight' : "Freight",
        'FreightFlat' : "Flat Freight",
        'NotSpecified' : "Not Specified"
        }
