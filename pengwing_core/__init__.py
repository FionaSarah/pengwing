"""
-------------------------------
Pengwing - eBay flipper manager
      2016 Fiona & Alice
-------------------------------

The core of Pengwing, it holds config settings and
database objects.
"""

import collections
import iso8601
import requests
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
import pengwing_core.settings
from .models import Base, Config

class EBaySearchException(Exception):
    pass

class PengwingCore:

    db_engine = None
    db_session = None

    def __init__(self):
        self._config = None
        # Connect to database
        self.db_engine = create_engine(settings.DATABASE_PATH)
        Base.metadata.bind = self.db_engine
        self.db_session = sessionmaker(bind=self.db_engine)()

    _config = None

    @property
    def config(self):
        # late load the config so it's automatic
        if self._config is None:
            self._config = {}
            for config in self.db_session.query(Config).all():
                self._config[config.id] = config.value
        return self._config

    @config.setter
    def config(self, val):
        self._config = val

    def query_ebay_by_keywords(self, keywords = "stuff", min_price = None, max_price = None, sort_type = None, payment_method = None, \
                               location = None, allow_pickup = True, currency_type = "GBP"):
        """Makes a request to eBay asking to search for items by a specific set of keywords.
        Returns a dictionary of item IDs to item info. Each set of item info is a dictionary
        that has the following keys:

        'item_url': str, URL to view the item
        'title': str, title of the listing
        'subtitle': str, subtitle shown under the title
        'image_url': str, URL to a thumbnail image of the listing
        'country': str, two letter country code where the item is from (for example: GB)
        'buy_it_now': bool, if buy it now is enabled
        'buy_it_now_price': int, how much an item costs to buy it now, in pence.
        'listing_type': str, one of AdFormat, Auction, AuctionWithBIN, Classified, FixedPrice, StoreInventory
        'start_time': DateTime, when the item listing started in UTC
        'end_time': DateTime, when the item listing will finish in UTC
        'bid_count': int, number of bids that have been made on this listing
        'current_price': int, current price in pence.
        'shipping_type': str, one of Calculated, CalculatedDomesticFlatInternational, Flat,
          FlatDomesticCalculatedInternational, Free, FreePickup, Freight, FreightFlat, NotSpecified
        'shipping_price', int, how much the shipping is in pence
        """
        # Build params
        params=collections.OrderedDict()
        params['keywords'] = keywords
        item_filter_num = 0
        if not min_price is None:
            filter_name = 'itemFilter('+str(item_filter_num) +').'
            params[filter_name + 'name'] = "MinPrice"
            params[filter_name + 'value'] = format(float(min_price / 100.0) , '.2f')
            params[filter_name + 'paramName'] = "Currency"
            params[filter_name + 'paramValue'] = currency_type
            item_filter_num += 1
        if not max_price is None:
            filter_name = 'itemFilter('+str(item_filter_num) +').'
            params[filter_name + 'name'] = "MaxPrice"
            params[filter_name + 'value'] = format(float(max_price / 100.0), '.2f')
            params[filter_name + 'paramName'] = "Currency"
            params[filter_name + 'paramValue'] = currency_type
            item_filter_num += 1
        if not payment_method is None:
            params[filter_name + 'name'] = "PaymentMethod"
            params[filter_name + 'value'] = payment_method
            item_filter_num += 1
        if not location is None:
            params[filter_name + 'name'] = "LocatedIn"
            params[filter_name + 'value'] = location
            item_filter_num += 1
        if not sort_type is None:
            params['SortOrder'] = sort_type
        # Get request object
        request = EBayRequestFindingAPI(environment=settings.EBAY_ENVIRONMENT,
                                        service="findItemsAdvanced",
                                        app_id=settings.EBAY_CREDENTIALS[settings.EBAY_ENVIRONMENT]['app_id'],
                                        params=params)
        response = request.do()
        ebay_items = collections.OrderedDict()
        # If our response actually came back with something
        if not 'findItemsAdvancedResponse' in response:
            return
        try:
            # Get out basic info
            search_url = str(response['findItemsAdvancedResponse'][0]['itemSearchURL'])
            total = int(response['findItemsAdvancedResponse'][0]['paginationOutput'][0]['totalEntries'][0])
            # only bother continuing if something in response
            if total == 0:
                return {}
            # Build up each item
            for item in response['findItemsAdvancedResponse'][0]['searchResult'][0]['item']:
                item_id = str(item['itemId'][0])
                # Collate basic info
                ebay_items[item_id] = {
                    'item_url':str(item['viewItemURL'][0]),
                    'title':str(item['title'][0]).replace('\n', '').replace('\r', ''),
                    'subtitle':None,
                    'image_url':None,
                    'country':str(item['country'][0]),
                    'buy_it_now':True if str(item['listingInfo'][0]['buyItNowAvailable'][0]) == 'true' else False,
                    'buy_it_now_price':0,
                    'listing_type': str(item['listingInfo'][0]['listingType'][0]),
                    'end_time': iso8601.parse_date(str(item['listingInfo'][0]['endTime'][0])),
                    'start_time': iso8601.parse_date(str(item['listingInfo'][0]['startTime'][0])),
                    'bid_count':0,
                    'current_price':0,
                    'shipping_type':None,
                    'shipping_price':0,
                    }

                # Get buy it now price out
                if ebay_items[item_id]['buy_it_now']:
                    ebay_items[item_id]['buy_it_now_price'] = int(float(item['listingInfo'][0]['convertedBuyItNowPrice'][0]['__value__']) * 100)

                # Get subtitle
                if 'subtitle' in item:
                    ebay_items[item_id]['subtitle'] = str(item['subtitle'][0])

                # Get the gallery URL
                if 'galleryURL' in item:
                    ebay_items[item_id]['image_url'] = item['galleryURL'][0]

                # Get bid info
                if 'sellingStatus' in item:
                    if 'bidCount' in item['sellingStatus'][0]:
                        ebay_items[item_id]['bid_count'] = int(item['sellingStatus'][0]['bidCount'][0])
                    ebay_items[item_id]['current_price'] = int(float(item['sellingStatus'][0]['convertedCurrentPrice'][0]['__value__']) * 100)

                # Get shipping info
                if 'shippingInfo' in item:
                    ebay_items[item_id]['shipping_type'] = str(item['shippingInfo'][0]['shippingType'][0])
                    # Check if we're allowing local pickup items to get through
                    if not allow_pickup and ebay_items[item_id]['shipping_type'] == "FreePickup":
                        del(ebay_items[item_id])
                        continue
                    if 'shippingServiceCost' in item['shippingInfo'][0]:
                        ebay_items[item_id]['shipping_price'] = int(float(item['shippingInfo'][0]['shippingServiceCost'][0]['__value__']) * 100)

                # If current price is over max we should get rid, this kills BINs too.
                if ebay_items[item_id]['shipping_price'] + ebay_items[item_id]['current_price'] > max_price:
                    del(ebay_items[item_id])
                    continue

        except KeyError as e:
            raise EBaySearchException("Exception searching for {}: Key error:{}".format(keywords, str(e)))
        # send back
        return ebay_items

class EBayRequestBase:

    service_name_header = ""

    def __init__(self, environment='sandbax', service='serviceName', app_id = 'AppID', params={}):
        self.environment = environment
        self.service = service
        self.app_id = app_id

        self.url = self.get_url()
        self.headers = self.get_request_headers()
        self.params = params

    def do(self):
        request = requests.get(self.url, headers=self.headers, params=self.params)
        request.raise_for_status()
        return request.json()

    def get_request_headers(self):
        return {'X-EBAY-SOA-SERVICE-NAME': self.service_name_header,
                'X-EBAY-SOA-OPERATION-NAME': self.service,
                'X-EBAY-SOA-SERVICE-VERSION': '1.0.0',
                'X-EBAY-SOA-GLOBAL-ID': settings.EBAY_GLOBAL_ID,
                'X-EBAY-SOA-SECURITY-APPNAME': self.app_id,
                'X-EBAY-SOA-RESPONSE-DATA-FORMAT': 'JSON'}

    def get_base_url(self):
        raise NotImplementedError

class EBayRequestFindingAPI(EBayRequestBase):
    service_name_header = "FindingService"
    def get_url(self):
        return settings.EBAY_FINDING_API[self.environment] + "/services/search/FindingService/v1"

class EBayRequestMerchandisingAPI(EBayRequestBase):
    service_name_header = "MerchandisingService"
    def get_url(self):
        return settings.EBAY_MERCHANDISING_API[self.environment] + "/"

class EBayRequestShoppingAPI(EBayRequestBase):
    service_name_header = "ShoppingService"
    def get_url(self):
        return settings.EBAY_SHOPPING_API[self.environment] + "/"

class EBayRequestTradingAPI(EBayRequestBase):
    service_name_header = "TradingService"
    def get_url(self):
        return settings.EBAY_TRADING_API[self.environment] + "/"

core = PengwingCore()
