NOTE: This project is completely defunct. Limitations on eBay's API stopped any further
development of this as a fully featured flipping bot. 
In the end it does serve to do automatic searches on eBay and cataloguing of which ones you
have viewed and checked out, but it does not do any direct bidding.


-----------------------
Pengwing - eBay Flipper
By Fiona & Alice
-----------------------

*******************
*** DESCRIPTION ***
*******************

Pengwing is an eBay flipper management suite, consisting of a bot and a web interface.
It's backed by a Postgres database.
The bot is designed to run as a daemon.

It is written in Python and Flask, with SQLAlchemy.


********************
*** INSTALLATION ***
********************

- Checkout Pengwing.

- Install PostgresSQL, create user and database.

- Create virtualenv for Pengwing and add the root of Pengwing to Python path.

  > mkvirtualenv pengwing
  > add2virtualenv /path/to/pengwing

- Install requirements file.

  > pip install -r requirements.txt

- Edit DATABASE_PATH in pengwing_core/settings.py so it uses the database you created earlier.

- Use the manage script to create the initial database tables.

  > ./manage.py init_database

- Start Pengwing bot as system daemon.


****************
*** UPDATING ***
****************

- Get latest version from git "git pull origin master"

- Make sure the 'sqlalchemy.url' setting in alembic.ini is the same as in settings.py.

- Run all outstanding migrations.

  > ./manage.py run_migrations

- Restart bot and web server.


*************
*** USAGE ***
*************

The bot will regularly search eBay for items as per the settings. These are viewable
through the web interface for approval or rejection. After approved the bot will bid
on items automatically.

Most settings are editable through the web interface.

There is also a command line management tool for doing various other tasks.
