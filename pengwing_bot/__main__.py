"""
-------------------------------
Pengwing - eBay flipper manager
      2016 Fiona & Alice
-------------------------------

The main code for the bot itself
"""

import time
import traceback
from datetime import datetime
from pengwing_core import core, EBaySearchException
from pengwing_core.models import SearchQuery, EBayItem

class PengwingBot():

    def __init__(self):
        # Run in a permanent loop
        while True:
            # Hack to force config to reload for this loop
            core.config = None

            # Do bids before searches
            # . . .

            # Get all search queries
            for query in core.db_session.query(SearchQuery).all():
                # Figure out how long it's been since last search
                mins_since_previous_search = ((datetime.utcnow() - query.last_searched).seconds) / 60
                if mins_since_previous_search < int(core.config['search_interval']):
                    continue

                # Do the search
                print("Searching for {}...".format(query.query))
                try:
                    items = core.query_ebay_by_keywords(
                        keywords = query.query,
                        min_price = 0,
                        max_price = query.max_bid,
                        sort_type = "StartTimeNewest",
                        payment_method = "PayPal",
                        location = "GB",
                        allow_pickup = False,
                        currency_type = "GBP"
                        )
                    if not len(items):
                        continue

                    # Iterate through all found items
                    num_new_items = 0
                    for item_id, item_info in items.items():
                        # Check it's not in the database
                        if core.db_session.query(EBayItem).get(item_id):
                            continue
                        print("Adding item to database - {}".format(item_info['title']))
                        core.db_session.add(
                                EBayItem(
                                    id=item_id,
                                    search_query=query,
                                    item_url=item_info['item_url'],
                                    title=item_info['title'],
                                    subtitle=item_info['subtitle'],
                                    image_url=item_info['image_url'],
                                    country=item_info['country'],
                                    buy_it_now=item_info['buy_it_now'],
                                    buy_it_now_price=item_info['buy_it_now_price'],
                                    listing_type=item_info['listing_type'],
                                    start_time=item_info['start_time'],
                                    end_time=item_info['end_time'],
                                    bid_count=item_info['bid_count'],
                                    current_price=item_info['current_price'],
                                    shipping_type=item_info['shipping_type'],
                                    shipping_price=item_info['shipping_price']
                                )
                            )
                        core.db_session.commit()
                        num_new_items+=1
                    print("{} new items added.".format(num_new_items))

                except EBaySearchException as e:
                    print((str(e)))
                    traceback.print_tb(e.__traceback__)
                finally:
                    # Update last search time
                    query.last_searched = datetime.utcnow()
                    core.db_session.commit()


            # Do everything every 10 seconds
            time.sleep(10)

if __name__ == '__main__':
    PengwingBot()
